<?php

declare(strict_types=1);

namespace App\Action\Product;

class GetCheapestProductsResponse
{
    const QUANTITY_CHEAPEST_PRODUCTS= 3;

    private $products;

    public function __construct(array $products)
    {
        $this->products = $products;
    }

    public function getProducts(): array
    {
        return array_slice($this->products, 0, self::QUANTITY_CHEAPEST_PRODUCTS);
    }
}