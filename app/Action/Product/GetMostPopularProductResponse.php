<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;

class GetMostPopularProductResponse
{
    private $products;

    public function __construct(Array $products)
    {
        $this->products = $products;
    }

    public function getProduct(): Product
    {
        return $this->products[0];
    }
}