<?php

namespace App\Http\Controllers;

use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetCheapestProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;

final class ProductController extends Controller
{
    private $getAllProductsAction;
    private $getMostPopularProductAction;
    private $getCheapestProductsAction;
    private $presenter;

    public function __construct(
        GetAllProductsAction $allProductsAction,
        GetMostPopularProductAction $mostPopularProductAction,
        GetCheapestProductsAction $cheapestProductsAction,
        ProductArrayPresenter $presenter
    ) {
        $this->getAllProductsAction = $allProductsAction;
        $this->getMostPopularProductAction = $mostPopularProductAction;
        $this->getCheapestProductsAction = $cheapestProductsAction;
        $this->presenter = $presenter;
    }

    public function getAllProducts()
    {
        $products = $this->getAllProductsAction->execute()->getProducts();

        return response()->json($this->presenter::presentCollection($products));
    }

    public function getPopularProducts()
    {
        $products = $this->getMostPopularProductAction->execute()->getProduct();

        return response()->json($this->presenter::present($products));
    }

    public function getCheapestProducts()
    {
        $products = $this->getCheapestProductsAction->execute()->getProducts();

        return view('cheap_products', [
            'products' => $this->presenter::presentCollection($products)
        ]);
    }
}