<?php

declare(strict_types=1);

namespace App\Repository;

class ProductRepository implements ProductRepositoryInterface
{
    private $products;

    public function __construct($products)
    {
        $this->products = $products;
    }

    public function findAll(): array
    {
        return $this->products;
    }

    public function sortByPrice(): array
    {
        $collection = collect($this->findAll());

        return $collection->sortBy('price')->values()->toArray();
    }

    public function sortByRating(): array
    {
        $collection = collect($this->findAll());

        return $collection->sortByDesc('rating')->values()->toArray();
    }
}
