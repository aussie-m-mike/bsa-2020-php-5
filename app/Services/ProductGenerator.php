<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Product;

class ProductGenerator
{
    /**
     * @return Product[]
     */
    public static function generate(): array
    {
        return [
            new Product(1, 'Coca-Cola', 26.90, 'url1', 4),
            new Product(2, 'Orange juice', 16.90, 'url2', 5),
            new Product(3, 'Tonic water', 5.90, 'url3', 3),
            new Product(4, 'Water', 7.90, 'url4', 2),
            new Product(5, 'Mirinda', 19.90, 'url5', 4),
            new Product(6, 'Kofola', 18.90, 'url6', 1)
        ];
    }
}