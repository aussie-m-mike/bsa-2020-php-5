<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
</head>
<body>
    <ul>
        @foreach($products as $product)
            <li>@include('product')</li>
        @endforeach
    </ul>
</body>
</html>